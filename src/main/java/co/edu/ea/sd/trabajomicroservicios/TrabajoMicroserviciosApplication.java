package co.edu.ea.sd.trabajomicroservicios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrabajoMicroserviciosApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrabajoMicroserviciosApplication.class, args);
	}

}
