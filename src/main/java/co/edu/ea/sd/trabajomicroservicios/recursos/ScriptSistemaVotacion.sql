create database sistemavotacion;
-- public.puestovotacion definition

-- Drop table

-- DROP TABLE public.puestovotacion;

CREATE TABLE public.puestovotacion (
	id varchar NOT NULL,
	nombrepuesto varchar NOT NULL,
	lugar varchar NOT NULL,
	municipio varchar NOT NULL,
	departamento varchar NOT NULL,
	CONSTRAINT puestovotacion_pkey PRIMARY KEY (id)
);


-- public.votante definition

-- Drop table

-- DROP TABLE public.votante;

CREATE TABLE public.votante (
	id varchar NOT NULL,
	nombrevotante varchar NOT NULL,
	puesto_votacion_id varchar NOT NULL,
	cambio bool NULL,
	voto bool NULL,
	CONSTRAINT votante_pkey PRIMARY KEY (id),
	CONSTRAINT votante_fk FOREIGN KEY (puesto_votacion_id) REFERENCES puestovotacion(id) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO public.votante (id,nombrevotante,puesto_votacion_id,cambio,voto) VALUES
	 ('2','juan','2',true,false),
	 ('3','camilo','2',true,false),
	 ('4','lucas','3',false,false),
	 ('1','sebastian','1',false,false);

INSERT INTO public.puestovotacion (id,nombrepuesto,lugar,municipio,departamento) VALUES
	 ('1','puesto1','colegio del carmen','Armenia','Quindio'),
	 ('2','puesto2','universidad del quindio','Armenia','Quindio'),
	 ('3','puesto3','universidad eam','Armenia','Quindio');