package co.edu.ea.sd.trabajomicroservicios.services;

import co.edu.ea.sd.trabajomicroservicios.model.entities.Puestovotacion;
import co.edu.ea.sd.trabajomicroservicios.repositories.PuestovotacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PuestovotacionService {

    @Autowired
    private PuestovotacionRepository puestovotacionRepository;

    public void crear(Puestovotacion puestovotacion){
        puestovotacionRepository.save(puestovotacion);
    }




}
