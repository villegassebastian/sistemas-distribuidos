package co.edu.ea.sd.trabajomicroservicios.model.entities;

import co.edu.ea.sd.trabajomicroservicios.model.entities.Puestovotacion;

import javax.persistence.*;


@Table(name = "votante")
@Entity
public class Votante {

    @Id
    private String id;

    @Column(name = "nombrevotante")
    private String nombrevotante;

    @Column(name = "cambio")
    private boolean cambio;

    @Column(name = "voto")
    private boolean voto;

    @ManyToOne
    @JoinColumn(name = "puesto_votacion_id")
    private Puestovotacion puestovotacion;

    public Votante(){
        this.cambio=false;
        this.voto=false;
    }

    public Votante(String id, String nombrevotante, boolean cambio, boolean voto, Puestovotacion puestovotacion) {
        this.id = id;
        this.nombrevotante = nombrevotante;
        this.cambio = cambio;
        this.voto = voto;
        this.puestovotacion = puestovotacion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombrevotante() {
        return nombrevotante;
    }

    public void setNombrevotante(String nombrevotante) {
        this.nombrevotante = nombrevotante;
    }

    public Puestovotacion getPuestovotacion() {
        return puestovotacion;
    }

    public void setPuestovotacion(Puestovotacion puestovotacion) {
        this.puestovotacion = puestovotacion;
    }

    public boolean isCambio() {

        return cambio;
    }

    public void setCambio(boolean cambio) {
        this.cambio = cambio;
    }

    public boolean isVoto() {
        return voto;
    }

    public void setVoto(boolean voto) {
        this.voto = voto;
    }
}
