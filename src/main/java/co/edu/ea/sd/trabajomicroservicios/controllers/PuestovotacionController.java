package co.edu.ea.sd.trabajomicroservicios.controllers;

import co.edu.ea.sd.trabajomicroservicios.model.entities.Puestovotacion;
import co.edu.ea.sd.trabajomicroservicios.model.entities.Votante;
import co.edu.ea.sd.trabajomicroservicios.services.PuestovotacionService;
import co.edu.ea.sd.trabajomicroservicios.services.VotantesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/puestovotacion")
public class PuestovotacionController {

    @Autowired
    private PuestovotacionService puestovotacionService;

    @Autowired
    private VotantesService votantesService;

    //guarda los puestos de votacion
    @PostMapping
    public void crear(@RequestBody Puestovotacion puestovotacion){


        puestovotacionService.crear(puestovotacion);
    }
    //listar los votantes de un puesto de votacion
    @GetMapping("/{id}/votantes")
    public List<Votante> votantesPuestovotacion(@PathVariable String id){
        return votantesService.listarVotantesPuestovotacion(id);
    }
}
