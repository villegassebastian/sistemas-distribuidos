package co.edu.ea.sd.trabajomicroservicios.services;

import co.edu.ea.sd.trabajomicroservicios.model.entities.Votante;
import co.edu.ea.sd.trabajomicroservicios.repositories.VotanteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VotantesService {

    @Autowired
    private VotanteRepository votanteRepository;

    public void crear(Votante votante) {
        votanteRepository.save(votante);
    }

    public List<Votante> listarVotantesPuestovotacion(String id) {
        Iterable<Votante> votantes = votanteRepository.votantesPuestovotacion(id);
        List result = new ArrayList();
        votantes.forEach(result::add);//for de votantes y le agrega cada elemento a resutl
        return result;
    }


    public Votante get(String id) {

        return votanteRepository.findById(id).orElse(null);
    }


    public void editarPuestoVotacionVotante(Votante votante) {

        Optional<Votante> listaVotante = votanteRepository.findById(votante.getId());


        if (listaVotante.isEmpty()) {
            System.out.printf("No existe el votante");
            throw new RuntimeException("No existe el votante");
        }
        for (int i = 1; i <= listaVotante.hashCode(); i++) {
            if (listaVotante.get().isCambio() == true) {
                System.out.printf("Al votante ya se le modifico el puesto de votacion");
                throw new RuntimeException("Al votante ya se le modifico el puesto de votacion");
            }
        }
        votanteRepository.save(votante);
    }

    public void yavoto(Votante votante) {

        Optional<Votante> listaVotante = votanteRepository.findById(votante.getId());


        if (listaVotante.isEmpty()) {
            System.out.printf("No existe el votante");
            throw new RuntimeException("No existe el votante");
        }
        for (int i = 1; i <= listaVotante.hashCode(); i++) {
            if (listaVotante.get().isCambio() == true) {
                System.out.printf("Al votante ya se le modifico el puesto de votacion");
                throw new RuntimeException("Al votante ya se le modifico el puesto de votacion");
            }else if (listaVotante.get().isVoto() == true){
                System.out.printf("El votante ya voto por un candidato");
                throw new RuntimeException("El votante ya voto por un candidato");
            }
        }
        for (int i = 1; i <= listaVotante.hashCode(); i++){
            votante.setNombrevotante(listaVotante.get().getNombrevotante());
        }
        votante.setVoto(true);
        votanteRepository.save(votante);
    }


}


