package co.edu.ea.sd.trabajomicroservicios.consumer;

import co.edu.ea.sd.trabajomicroservicios.model.entities.Votante;
import co.edu.ea.sd.trabajomicroservicios.model.msj.VotanteMessage;
import co.edu.ea.sd.trabajomicroservicios.services.VotantesService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class VotanteQueueConsumer {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private VotantesService votantesService;

    @KafkaListener(topics = "2aml5a5q-votos")
    public void ListenVotantesQueue(String message) throws JsonProcessingException {
        System.out.println(message);

        VotanteMessage v= objectMapper.readValue(message, VotanteMessage.class);

        votantesService.yavoto(new Votante(v.getId(),v.getNombre(),v.isCambio(),v.isVoto(),v.getPuestovotacion()));
    }
}
