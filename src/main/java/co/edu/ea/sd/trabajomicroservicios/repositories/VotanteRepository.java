package co.edu.ea.sd.trabajomicroservicios.repositories;

import co.edu.ea.sd.trabajomicroservicios.model.entities.Votante;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VotanteRepository extends CrudRepository<Votante, String> {

   @Query("SELECT v FROM Votante v where v.puestovotacion.id = :id")
    public List<Votante> votantesPuestovotacion(String id);




}
