package co.edu.ea.sd.trabajomicroservicios.controllers;

import co.edu.ea.sd.trabajomicroservicios.model.entities.Votante;
import co.edu.ea.sd.trabajomicroservicios.services.VotantesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/votante")
public class VotanteController {

    @Autowired
    private VotantesService votantesService;

    //Guarda el votante
    @PostMapping
    public void crear(@RequestBody Votante votante){
        votantesService.crear(votante);
    }

    //consultar puesto de votacion por el id del votante
    @GetMapping("/{id}/puestovotacion")
    public Votante get(@PathVariable String id){
        return votantesService.get(id);
    }

    //modifica el votante, y validad que no se pueda modificar si el puesto de votacion ya habia sido cambiado
    @PutMapping("/{id}")
    public void edit(@PathVariable String id,@RequestBody Votante votante){

        votante.setId(id);
        votante.setCambio(true);
        votantesService.editarPuestoVotacionVotante(votante);
    }


}
