package co.edu.ea.sd.trabajomicroservicios.repositories;

import co.edu.ea.sd.trabajomicroservicios.model.entities.Puestovotacion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PuestovotacionRepository extends CrudRepository<Puestovotacion, String> {


}
